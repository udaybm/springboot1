package com.SpringMongo.SpringMongo.Repo;

import com.SpringMongo.SpringMongo.Models.Model;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoRepo extends MongoRepository<Model, Long> {
}
