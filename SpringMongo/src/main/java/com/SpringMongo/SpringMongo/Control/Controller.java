package com.SpringMongo.SpringMongo.Control;

import com.SpringMongo.SpringMongo.Models.Model;
import com.SpringMongo.SpringMongo.Repo.MongoRepo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.util.aggregation.TestAggregationContext;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class Controller {
    @Autowired
   private MongoRepo repo;
    public static final Logger log = LoggerFactory.getLogger(TestAggregationContext.class);

    @PostMapping("/addStudent")
    public Model postmethod(@RequestBody Model model)
    {
        log.info("Model object is :-,{}",model);
       return repo.save(model);
    }
    @GetMapping("/getdata")
    public List<Model> getmethod()
    {
        //repo.findById(id);
       return repo.findAll();
    }
    @GetMapping("/getbyid")
    public Optional<Model> getbyid(@RequestParam Long id)
    {
        return repo.findById(id);
    }
    @DeleteMapping
    public void deletebyid(@RequestBody Long id)
    {
        repo.deleteById(id);
    }
}
